@extends('master')

@section('judul')
    Halaman Tambah Cast
@endsection

@section('content')
<h2>Tambah Data</h2>
<form action="/cast/{{ $cast->id }}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control" name="nama" value="{{old('nama') ?? $cast->nama }}" id="nama" placeholder="Masukkan Nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="umur">umur</label>
        <input value="{{old('umur') ?? $cast->umur }}" type="number" class="form-control" name="umur" " id="umur" placeholder="Masukkan Umur">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form group">
        <label>Bio</label>
        <textarea name="bio" class="form-control" cols="30" " rows="10">{{old('bio') ?? $cast->bio }}</textarea>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>        
        @enderror
    </div>
        
    <button type="submit" class="btn btn-primary mt-3">Update</button>
</form>
   
@endsection