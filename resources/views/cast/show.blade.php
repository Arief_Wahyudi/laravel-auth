@extends('master')

@section('judul')
    Halaman Detail
@endsection

@section('content')

<div class="card-body">
    <label >No :</label>
    <p>{{$cast->id}}</p>

    <label >Nama :</label>
    <p>{{$cast->nama}}</p>

    <label >Umur :</label>
    <p>{{$cast->umur}}</p>

    <label >Bio :</label>
    <p>{{$cast->bio}}</p>
</div>

@endsection