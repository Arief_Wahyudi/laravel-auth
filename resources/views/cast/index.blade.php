@extends('master')

@section('judul')
    Halaman Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary mb-3">Tambah</a>
        <table class="table table-bordered">
            <thead class="thead-light">
              <tr>
                <th scope="col" class="text-center">NO</th>
                <th scope="col" class="text-center">NAMA</th>
                <th scope="col" class="text-center">UMUR</th>
                <th scope="col" class="text-center">BIO</th>
                <th scope="col" class="text-center">ACTIONS</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($casts as $key=>$value)
                    <tr>
                        <td class="text-center">{{$key + 1}}</th>
                        <td class="text-center">{{$value->nama}}</td>
                        <td class="text-center">{{$value->umur}}</td>
                        <td class="text-center">{{$value->bio}}</td>
                        <td class="text-center">
                            <a href="/cast/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            
                            <form action="/cast/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE') 
                                <input type="submit" class="btn btn-danger my-1" value="Delete">  
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5" class="text-center">No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
@endsection